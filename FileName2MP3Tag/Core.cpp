#include "stdafx.h"
#include "Core.h"
#pragma warning(disable:4996)


void toBack(std::fstream* file, unsigned long start, unsigned long len){
	char buf[4096];
	unsigned long moved = 4096, end = len - start;
	while (moved <= end){
		file->read(buf, 4096);
		file->seekp(len - moved);
		moved += 4096;

		file->write(buf, 4096);
	}

	if (moved != end){
		file->read(buf, len - moved);
		file->seekp(start);

		file->write(buf, len - moved);
	}
}
char Work(char* path){
	char* tmp = "G:\\Sleep Away.mp3";
	path = "G:\\��.mp3";
	std::fstream file(path, std::ios_base::in | std::ios_base::out | std::ifstream::ate | std::ifstream::binary);
	unsigned int len = 0;
	
	if (file.is_open() == false || (len = (unsigned int)file.tellg()) < 33) {
		file.clear();  
		return 0;
	}
	
	file.clear();
	file.seekg(0);
	MP3::type MP3Type = getMP3Type(&file, len);
	std::string name = getFileName(path);
	ID3v2(&file, name, path);

	switch (MP3Type)
	{
		case MP3::ID3v1_extend:
			ID3v1_extend(&file, getFileName(path), len);
		case MP3::ID3v1:
			ID3v1(&file, getFileName(path), len);
			break;

		case MP3::ID3v2:
			ID3v2(&file, getFileName(path), path);
			break;

		default:
			return 0;
			break;
	}
}
MP3::type getMP3Type(std::fstream* file, unsigned int len){
	char tmp[4];
	file->seekg(len - 355);
	file->read(tmp, 4);
	if (strncmp(tmp, "TAG+", 4) == 0) return MP3::ID3v1_extend;

	file->seekg(len - 128);
	file->read(tmp, 3);
	if (strncmp(tmp, "TAG", 3) == 0) return MP3::ID3v1;

	file->seekg(0);
	file->read(tmp, 3);
	if (strncmp(tmp, "ID3", 3) == 0) return MP3::ID3v2;
	
	return MP3::UnKnown;
}

std::string getFileName(char* path){
#ifdef _WIN32
	char sep = '\\';
#else
	char sep = '/';
#endif
	char* path_tmp = new char[strlen(path) + 1];
	strcpy(path_tmp, path);
	char* tmp = path_tmp;
	char* res = path_tmp;
	while (*tmp != 0)
	{
		if (*tmp == sep) res = tmp;
		++tmp;
	}

	while (tmp != res)
	{
		--tmp;
		if (tmp[0] == '.') {
			tmp[0] = '\0'; 
			break;
		}
	}

	++res;
	std::string to_return = std::string(res);
	delete path_tmp;
	return to_return;
}
char ID3v1_extend(std::fstream* file, std::string name, unsigned int len){
	char to_write[60];
	std::memset(to_write, 0, 60);

	if (strlen(name.c_str()) < 60)
		strcpy(to_write, name.c_str());
	else
		strncpy(to_write, name.c_str(), 59);

	file->seekp(len - 351);
	*file << to_write;

	return 0;
}
char ID3v1(std::fstream* file, std::string name, unsigned int len){
	char to_write[30];
	std::memset(to_write, 0, 30);

	if (strlen(name.c_str()) < 30)
		strcpy(to_write, name.c_str());
	else
		strncpy(to_write, name.c_str(), 29);

	file->seekp(len - 125);
	*file << to_write;

	return 0;
}
char ID3v2(std::fstream* file, std::string name, char* path){
	std::fstream backup((std::string(path) + ".bak").c_str(), std::ios::binary | std::ios::trunc | std::ios::out);
	backup.seekp(0);
	file->clear();
	file->seekg(0);
	
	backup << file->rdbuf();
	
	file->clear();
	file->seekg(0);

	backup.close();

	struct MP3::ID3v2_info id3v2;
	ID3v2_tag(file, &id3v2);
	
	unsigned long len = id3v2.length;
	std::reverse((char *)&len, (char *)&len + 4);

	if (id3v2.length >= name.size()){
		char* to_write = new char[id3v2.length + 1];
		memset(to_write, 0, id3v2.length);
		memcpy(to_write, name.c_str(), name.size());
		file->seekp(id3v2.pos - 7);
		
		file->write((char*)&len, 4);
		file->seekp((unsigned long)file->tellp() + 3);
		file->write(to_write, id3v2.length);
	}
	else{
		char* to_write = new char[name.size() + 1];
		memset(to_write, 0, name.size());
		memcpy(to_write, name.c_str(), name.size());
		file->seekp(id3v2.pos - 7);

		file->write((char*)&len, 4);
		file->seekp((unsigned long)file->tellp() + 3);
		file->write(to_write, id3v2.length);
	}

	return 0;
}
void ID3v2_tag(std::fstream* file, struct MP3::ID3v2_info* id3v2){
	char tmp[8];
	file->read(tmp, 8);
	
	if (tmp[0] != 0x49 || tmp[1] != 0x44 || tmp[2] != 0x33)
		return;

	long* len_tmp = new long();
	unsigned long* lentmp = new unsigned long();

	file->seekg(10);
	while (file->eof() == false && file->good()){
		file->read(tmp, 8);
		std::reverse(tmp + 4, tmp + 8);
		if (strncmp(tmp, "TIT2", 4) == 0) break;

		file->seekg((unsigned long)file->tellg() + *(unsigned long*)(tmp + 4) + 2);
	}

	id3v2->length = *(unsigned long*)(tmp + 4) - 1;
	id3v2->pos = (unsigned long)file->tellg() + 3;

	printf("%d", file->tellg());
}