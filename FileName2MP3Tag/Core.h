#include "stdafx.h"
namespace MP3{
	enum type
	{
		UnKnown = 0,
		ID3v1,
		ID3v1_extend,
		ID3v2
	};
	struct ID3v2_info{
		unsigned long pos;
		unsigned long length;
	};
}
char Work(char* path);
MP3::type getMP3Type(std::fstream* file, unsigned int len);

std::string getFileName(char* path);
char ID3v1_extend(std::fstream* file, std::string name, unsigned int len);
char ID3v2(std::fstream* file, std::string name);
char ID3v1(std::fstream* file, std::string name, unsigned int len);
void ID3v2_tag(std::fstream* file, struct MP3::ID3v2_info* id3v2);
char ID3v2(std::fstream* file, std::string name, char* path);
void toBack(std::fstream* file, char* start, char* end);